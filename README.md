# UNO-Multiplayer
The project is a browser based uno multiplayer game. It was developed on the server side with rust and communicates via websocket with the client. On the client side we worked with raw javascript, html and css. The client files can be hosted statically on a webserver (for example nginx). The final result can be visited and played at [io-games.de](https://uno.fscode.de/uno/). Have fun with your friends and enjoy the game.

![uno_logo](/client/uno/img/uno_logo.png)

## Features
* complete implementation of all standard uno rules
* different rooms with parallel game sessions
* text chat
* a login screen
* waiting rooms before the game starts
* up to 100 players in a single game session

## In progress
* some bug fixes with websocket
* sound integration
* smartphone optimization
* ....

## Available at
* [io-games.de/uno](https://io-games.de/uno)
* [uno.io-games.de](https://uno.io-games.de)
* [uno.fscode.de/uno](https://uno.fscode.de/uno)

# Deploy

## Server
```
cargo build --release
```

the executable file can be found under ``./server/target/release/uno-game-server`` afterwards.
In the next step you need a port forwarding of Port 8025 to your specific domain.

## Client

The ./client folder has to be reachable under a sub domain over a webserver. And in the client.js (Line 32) file, you have to edit the path for the websocket connection to your specific needs. For Example `const socket = Socket.create("wss://uno.io-games.de/uno-ws");`
