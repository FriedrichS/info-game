//GLOBAL CONSTANTS
//let STATE = 1;                  //1 = Lobby 2 = Wait Player 3 = Game

/** CLIENT
 */

/** SERVER
 *  @todo reinschmeißen von karten
 *  @todo nicht mehrfach anmeldung mit gleichem name
 */

//EVENTHANDLER
window.addEventListener("load", onload, false);
window.onresize = () => {
    const canvas = document.getElementById("ctx");
    canvas.width = window.innerWidth * 0.6;
    canvas.height = window.innerHeight;
};
window.onbeforeunload = () => {
    return null;
};


function onload() {
    //Canvas wird fensterfüllend gesetzt
    // $("#ctx").width  = window.innerWidth;
    // $("#ctx").height = window.innerHeight;
}

$(document).ready(() =>{
    const tempName = localStorage.getItem('name');
    const socket = Socket.create("wss://uno.fscode.de/uno-ws");// ws://146.52.110.194:8025');
    const lobby = Lobby.create(socket, tempName);
    

    $("#btnNewRoom").click(() => {
        lobby.newRoom();
    });

    $("#roomBoard").on("click", ".roomList", (event) => {
        lobby.joinRoom(event);
    });

});