class Chat {
    constructor(socket, displayElement, inputElement) {
        this.socket = socket;
        this.displayElement = displayElement;
        this.inputElement = inputElement;
    }

    /**
     * Factory Method to create a Chat class.
     * @param {Socket}
     * @param {String}
     * @return {Chat}
     */
    static create(socket, displayElementID, inputElementID) {
        const displayElement = document.getElementById(displayElementID)
        displayElement.innerHTML = '';
        const inputElement = document.getElementById(inputElementID)
        const chat = new Chat(socket, displayElement, inputElement)
        chat.init();
        return chat;
    }

    /**
     * This is a test change
     */
    init() {
        this.inputElement.addEventListener('keydown', this.onInputKeyDown.bind(this))
    }

    onInputKeyDown(event) {
        const text = this.inputElement.value;
        if(event.keyCode === 13 && text !== "") {
            this.inputElement.value = '';
            this.socket.send(Constants.SOCKET_CHAT_EMIT, {msg: text});
            console.log(text);
        }
    }

    onChatReceive(packet) {
        const element = document.createElement('li')
        element.setAttribute('id', 'chat-li');
        element.appendChild(document.createTextNode(`${packet.payload.name}: ${packet.payload.msg}`))
        this.displayElement.appendChild(element)
        $("#chat-display").scrollTop($("#chat-display")[0].scrollHeight);
    }

}