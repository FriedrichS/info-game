const Constants = {

    SOCKET_GAME_UPDATE: "room",         //in der Wartezeit die Infos über den aktuellen Raum
    SOCKET_GAME_START: "room_start",
    SOCKET_GAME_INFO: "room_info",
    SOCKET_GAME_END: "room_end",
    SOCKET_GAME_ACK: "join_room_ack",
    SOCKET_GAME_LEAVE: "leave_room",
    SOCKET_GAME_FORCE_WIN: "force_win",

    SOCKET_CARD: "card",
    SOCKET_LOBBY: "lobby",
    SOCKET_JOIN_ACK: "join_ack",
    SOCKET_JOIN: "join",
    SOCKET_CREATE_ROOM: "create_room",
    SOCKET_JOIN_GAME:"join_room",
    SOCKET_CHAT_EMIT: "chat_emit",
    SOCKET_CHAT_UPDATE: "chat_update",
    SOCKET_FORCE_ROOM_START: "room_start_force",
    SOCKET_PLAY_SOUND: "play_sound",

    CARD_WIDTH: 240,
    CARD_HEIGHT: 360,
    CARD_COUNT: 52,
    
    CARD_ID_DRAW: 0,
    CARD_ID_BACK: 1,
    CARD_ID_CHOOSE_COLOR: [13, 26, 39, 52],
    CARD_COLORS: ["red", "blue", "green", "yellow"],

    SCALE: 0.5,

    DRAWING_IMG_BASE_PATH: "img/",
    DRAWING_IMG_FRONTCARDS: "tiles/tiles_{}.png",
    DRAWING_IMG_BACKCARD: "UNO-Back-5.png",
    DRAWING_IMG_DRAWCARD: "UNO-Draw.png",
    DRAWING_IMG_BACKGROUND_RED: "bg_red.png",
    DRAWING_IMG_BACKGROUND_GREEN: "bg_green.png",

    WIDTH_BASE: 1440,

    SOUND_BASE_PATH: 'sfx/',
    SOUND_PLACE: 'cardPlace.mp3',
    SOUND_SHUFFLE: 'cardShuffle.mp3',
    SOUND_DRAW: 'cardDraw.mp3',
    SOUND_INVALID: 'invalidCard.mp3',
    SOUND_CHAT: 'chatMessage.mp3',

    SOCKET_OK: 1,

    STATE_LOBBY: 0,
    STATE_WAIT: 1,
    STATE_GAME: 2,

    STATE_GAME_WAIT: 0,
    STATE_GAME_IN: 1,
    STATE_GAME_END: 2,

    KEY_LEFT: 37,
    KEY_RIGHT: 39,
    KEY_UP: 38,
    KEY_DOWN: 40,

    KEY_ENTER: 13,
    KEY_ESC: 27,

    FONT_BASE: 1440,
    FONT_SIZE_SMALL: 45,
    FONT_SIZE_MEDIUM: 50,
    FONT_SIZE_LARGE: 350,

    FONT_STANDARD: 'Roboto'

}

Number.prototype.mod = function(n) {
    return ((this%n)+n)%n;
};

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

function getNormID(id) {
    return ((id-1).mod(Constants.CARD_COUNT))+1;
}