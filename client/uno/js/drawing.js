class Drawing {
    constructor(context, images, drawimg, backimg, background_red, background_green) {
        this.context = context;
        this.images = images;
        this.draw_img = drawimg;
        this.back_img = backimg;
        this.background_red = background_red;
        this.background_green = background_green;

        this.width = context.canvas.width;
        this.height = context.canvas.height;
        this.scale = (this.width / Constants.WIDTH_BASE)*0.5;
        this.context.save();

        this.context.scale(this.scale, this.scale);

    }

    static create(canvas) {
        const context = canvas.getContext('2d');
        context.canvas.width = window.innerWidth * 0.6;
        context.canvas.height = window.innerHeight;
        

        const images = []
        const backimg = new Image();
        backimg.src = Constants.DRAWING_IMG_BASE_PATH + Constants.DRAWING_IMG_BACKCARD;
        const drawimg = new Image();
        drawimg.src = Constants.DRAWING_IMG_BASE_PATH + Constants.DRAWING_IMG_DRAWCARD;
        const background_red = new Image();
        background_red.src = Constants.DRAWING_IMG_BASE_PATH + Constants.DRAWING_IMG_BACKGROUND_RED;

        const background_green = new Image();
        background_green.src = Constants.DRAWING_IMG_BASE_PATH + Constants.DRAWING_IMG_BACKGROUND_GREEN;

        for(let i = 1; i < Constants.CARD_COUNT+1; i++) {
            const path = Constants.DRAWING_IMG_BASE_PATH + Constants.DRAWING_IMG_FRONTCARDS.replace("{}",i);
            images[i] = new Image();
            images[i].src = path;
        }

        return new Drawing(context, images, drawimg, backimg, background_red, background_green);
    }

    clear() {
        this.context.clearRect(0,0 , this.width*(1/this.scale), this.height*(1/this.scale));
    }

    /**
     * @param {*} self 
     * @param {*} players 
     * @param {*} turn 
     * @param {*} pile_top 
     */
    drawGameScreen(self, players, turn, pile_top, time, draw_count, stackcount) {
        stackcount = 5;

        if(this.images[Constants.CARD_COUNT].width !== 0) {
            const countW = this.width/this.background_red.width+1;
            const countH = this.height/this.background_red.height+1;
    
            for(let i = 0; i < countW; i++) {
                for(let j = 0; j < countH; j++) {
                    (self.id === turn) ? this.context.drawImage(this.background_green, 255*i, 255*j) : this.context.drawImage(this.background_red, 255*i, 255*j);
                }
            }
    
    
            //EIGENE KARTEN
            let originX = (this.width-0.3125*Constants.CARD_WIDTH*(self.cards.length+1))/2;
            let originY = (this.height-Constants.CARD_HEIGHT/2);
    
    
            //mit umrandung
            if(turn == self.id) {
                for(let i = 0; i < self.cards.length; i++) {
                    if(i < self.selectedCard) {
                        this.drawCard(self.cards[i],originX + i*Constants.CARD_WIDTH*0.3125,originY, true)
                    } else if(i > self.selectedCard) {
                        this.drawCard(self.cards[i],originX + i*Constants.CARD_WIDTH*0.3125 + Constants.CARD_WIDTH*(1-0.3125) ,originY, true)
                    } else {
                        this.drawCard(self.cards[i],originX + i*Constants.CARD_WIDTH*0.3125 ,originY, true)
        
                        if(self.moves.some(c =>  (c === self.cards[i]))) {
                            this.context.strokeStyle = 'green';
                        } else {
                            this.context.strokeStyle = 'red';
                        }
        
                        this.context.lineWidth = 10;
        
        
                        this.context.strokeRect((originX + i*Constants.CARD_WIDTH*0.3125)-Constants.CARD_WIDTH/2, originY-Constants.CARD_HEIGHT/2, Constants.CARD_WIDTH-5, Constants.CARD_HEIGHT-5)
        
                    }
                }
            } else {
                for(let i = 0; i < self.cards.length; i++) {
                   this.drawCard(self.cards[i],originX + i*Constants.CARD_WIDTH*0.3125,originY, true)
                }                
            }  


            //DRAW CARD
            this.drawCard(0, (originX + ((self.cards.length*0.3125)*Constants.CARD_WIDTH)+ Constants.CARD_WIDTH*(1-0.3125)),originY, true, Constants.CARD_ID_DRAW);
    
            if(self.selectedCard == (self.cards.length) && turn == self.id) {
                this.context.strokeStyle = 'green';
                this.context.lineWidth = 10;
                this.context.strokeRect((originX + ((self.cards.length*0.3125)*Constants.CARD_WIDTH)+ Constants.CARD_WIDTH*(1-0.3125))-Constants.CARD_WIDTH/2, originY-Constants.CARD_HEIGHT/2, Constants.CARD_WIDTH+3, Constants.CARD_HEIGHT-5)
            }
            //SELCETED CARD
    
            //this.drawCard(self.cards[self.selectedCard],originX + self.selectedCard*Constants.CARD_WIDTH*0.3125,originY-Constants.CARD_HEIGHT/2, true)
    
            //SELECTOR UM DIE KARTEN
            // if(!turn) {
            //     if(self.moves.some(c =>  (c === self.cards[self.selectedCard]))) {
            //        this.context.strokeStyle = 'green';
            //     } else {
            //         this.context.strokeStyle = 'red';
            //     }
    
    
            //     this.context.lineWidth = 10;
            //     //this.context.strokeRect((originX + self.selectedCard*Constants.CARD_WIDTH)-Constants.CARD_WIDTH/2, originY-Constants.CARD_HEIGHT/2, Constants.CARD_WIDTH, Constants.CARD_HEIGHT)
    
            // }
    
            this.context.strokeRect(this.width-10, this.height-10,10,10);
    
            //CHOOSE COLOR
            const nid = ((self.cards[self.selectedCard]-1).mod(Constants.CARD_COUNT))+1;
            if((Constants.CARD_ID_CHOOSE_COLOR.some(c => (c === nid))) && turn == self.id) {
                this.context.fillStyle = Constants.CARD_COLORS[self.selectedColor];
                this.context.fillRect((originX + self.selectedCard*Constants.CARD_WIDTH*0.3125)-Constants.CARD_WIDTH/4, originY-Constants.CARD_HEIGHT/2 - 30 - Constants.CARD_WIDTH/2, Constants.CARD_WIDTH/2,Constants.CARD_WIDTH/2)
                this.context.strokeStyle = 'black';
                this.context.lineWidth = 10;
                this.context.strokeRect((originX + self.selectedCard*Constants.CARD_WIDTH*0.3125)-Constants.CARD_WIDTH/4, originY-Constants.CARD_HEIGHT/2 - 30 - Constants.CARD_WIDTH/2, Constants.CARD_WIDTH/2,Constants.CARD_WIDTH/2)
    
            }
    
    
            // // FREMDE KARTEN
            // originX = 0;
            // originY = 0;
    
            // for(let i = 0; i < players.length; i++){
            //     for(let j = 0; j < players[i].cards; j++){
            //         if(j > 4) {
            //             this.context.fillStyle = "#FFF";
            //             this.context.textAlign = "center"
            //             this.context.font = "250px Roboto";
            //             this.context.fillText(players[i].cards, j * Constants.CARD_WIDTH/6, i * Constants.CARD_HEIGHT);
            //             break;
            //         } 
            //         this.context.drawImage(this.back_img, j * Constants.CARD_HEIGHT/6, i * Constants.CARD_HEIGHT);
            //     }
            // }
    
    
            // LADEBALKEN
            this.context.lineWidth = 6;
            this.context.strokeStyle = "black";
            this.context.strokeRect((this.width - this.width/2)/2 ,40,this.width/2,40);  //Stroke
            ((time.countdown/time.move_time) > 0.4 ) ? this.context.fillStyle = "black" : this.context.fillStyle = "yellow";
            if ((time.countdown/time.move_time) < 0.2 ) this.context.fillStyle = "red";

            this.context.fillRect((this.width - this.width/2)/2 + 3,43, this.width/2 * (time.countdown/time.move_time) - 3,35);  //Fill
    
    
            //KARTE IN DER MITTE
            // originX = (this.width*(1/Constants.SCALE)-Constants.CARD_WIDTH)/2;
            // originY = (this.height*(1/Constants.SCALE)-Constants.CARD_HEIGHT)/2;

            //STACK
            for(let i = stackcount; i > 0; i--) {
                this.drawCard(pile_top.id,this.width/2,this.height/2+(i-1)*5, true);
            }
    
            this.drawCard(pile_top.id,this.width/2,this.height/2, true);




            // FARB ANZEIGE IN DER MITTE
            if((pile_top.color != "none") && (Constants.CARD_ID_CHOOSE_COLOR.some(c => (c === getNormID(pile_top.id))))) {
                this.context.fillStyle = pile_top.color;
                this.context.fillRect(this.width/2 -Constants.CARD_WIDTH/4, this.height/2+Constants.CARD_HEIGHT/2 + 30, Constants.CARD_WIDTH/2,Constants.CARD_WIDTH/2)
                this.context.strokeStyle = 'black';
                this.context.lineWidth = 10;
                this.context.strokeRect(this.width/2 -Constants.CARD_WIDTH/4, this.height/2+Constants.CARD_HEIGHT/2 + 30 , Constants.CARD_WIDTH/2,Constants.CARD_WIDTH/2)                
            }


            //DRAW COUNT / ANZAHL DER ZIEHKARTEN
            if(draw_count > 0) {
                this.context.fillStyle = "white";
                this.drawText("+" + draw_count, this.width/2,this.height/2 - Constants.CARD_HEIGHT,true, Constants.FONT_SIZE_LARGE);
            }


            //GRÜNE UMRANDUNG
            if(self.id === turn) {

                //this.drawText("Its your turn", this.width/2, Constants.CARD_HEIGHT, true, Constants.FONT_SIZE_MEDIUM)

                this.context.strokeStyle="green";
                const pxw = 20;

                this.context.lineWidth=pxw;
                this.context.strokeRect(pxw/2,pxw/2,this.width-pxw,this.height-pxw);
            }

            //NACHZIEHSTAPEL

            //STACK
            for(let i = stackcount; i > 0; i--) {
                this.drawCard(0,this.width/2+1.5*Constants.CARD_WIDTH,this.height/2+(i-1)*5, true, Constants.CARD_ID_BACK);
            }

            this.drawCard(0,this.width/2+1.5*Constants.CARD_WIDTH,this.height/2, true, Constants.CARD_ID_BACK);


    
            
            //this.context.scale(-Constants.SCALE, -Constants.SCALE);
        }

    }

    drawWinnerScreen(self, winner_id, winner_name) {

        this.drawText("Press ENTER to exit the screen", this.width/2, 100, true, Constants.FONT_SIZE_MEDIUM);


        if(self.id === winner_id) {
            this.context.fillStyle = "green";
            this.drawText("You won the game", this.width/2, this.height/2, true, Constants.FONT_SIZE_MEDIUM);

        } else {

            this.drawText(winner_name + " won the game", this.width/2, this.height/2, true, Constants.FONT_SIZE_MEDIUM);

        }

    }

    drawWaitingScreen(max_players, players) {
        this.drawText("Waiting for players (" + players.length +"/"+ max_players + ")", this.width/2, this.height/2, true, Constants.FONT_SIZE_MEDIUM);
        this.drawText("Press ENTER to start the game", this.width/2, 100, true, Constants.FONT_SIZE_MEDIUM);
        this.drawText("Press ESC to exit room", this.width/2, this.height-100, true, Constants.FONT_SIZE_MEDIUM);
    }

    updateDimensions() {
        this.context.restore();
        this.context.save();
        this.scale =  (this.context.canvas.width / Constants.WIDTH_BASE)*0.5;
        this.width = this.context.canvas.width * (1/this.scale);
        this.height = this.context.canvas.height* (1/this.scale);
        this.context.scale(this.scale, this.scale);
    }


    drawText(text, x, y, centered=false, fontsize=Constants.FONT_SIZE_MEDIUM) {
        const ratio = fontsize / Constants.FONT_BASE;
        const size = this.width*this.scale * ratio;
        this.context.font = size + 'px ' + Constants.FONT_STANDARD;

        if(centered){
            this.context.textBaseline  = 'middle';
            this.context.textAlign = 'center';
            this.context.fillText(text, x,y);
        } else {
            this.context.fillText(text, x,y);
        }
    }

    drawCard(id, x, y, centered=false, special=-1) {
        if(centered){
            const nid = ((id-1).mod(Constants.CARD_COUNT))+1;
            const width = this.images[nid].width;
            const height = this.images[nid].height;
            if(special === -1) {
                this.context.drawImage(this.images[nid],(x-width/2),(y-height/2));
            } else if (special === Constants.CARD_ID_BACK) {
                this.context.drawImage(this.back_img,x-width/2,y-height/2);
            } else if (special === Constants.CARD_ID_DRAW) {
                this.context.drawImage(this.draw_img,x-width/2,y-height/2);
            }
        } else {
            if(special === -1) {
                this.context.drawImage(this.images[nid],x,y);
            } else if (special === Constants.CARD_ID_BACK) {
                this.context.drawImage(this.back_img,x,y);
            } else if (special === Constants.CARD_ID_DRAW) {
                this.context.drawImage(this.draw_img,x,y);
            }
        }
    }
}