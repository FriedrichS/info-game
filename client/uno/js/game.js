class Game {
    constructor(socket, drawing, playerboard, id, name, room_name, sound) {
        this.drawing = drawing;
        this.socket = socket;
        this.playerboard = playerboard;
        this.sound = sound;

        this.id = id;       //Player ID
        this.name = name;   //Player Name

        this.room_name = room_name;     //Room Name
        
        this.self;
        this.players = []; //= [{id: 0, cards: 5, name: "Spieler 1"}, {id: 2, cards: 8, name: "Spieler 2"}]; //[];
        this.players_card_count = [];
        this.max_players;

        // this.cardImgs = [];
        // this.tileCount = Constants.TILE_COUNT;

        this.state; 

        this.cardWidth = Constants.CARD_WIDTH;
        this.cardHeight = Constants.CARD_HEIGHT;
        this.turn;

        this.draw_count;

        // this.scale = Constants.SCALE;

        this.pile_top;

        this.move_time;
        this.countdown;
        this.start_time;

        this.animationFrameId = null
        this.lastUpdateTime = 0
        this.deltaTime = 0    

        this.stackcount = 0;


        this.winner_id;
        this.winner_name;

        this.parent;
    }

    // loadImages() {
    //     for(let i = 0; i < this.tileCount; i++) {
    //         let path = "img/tiles/tiles_" + (i+1)+".png";
    //         this.cardImgs[i] = new Image();
    //         this.cardImgs[i].src = path;
    //     }

    //     this.cardImgs[this.tileCount] = new Image();    //RÜCKSEITE
    //     this.cardImgs[this.tileCount].src = "img/UNO-Back.png"
    // }

    // getTile(id) {
    //     return this.cardImgs[id];
    // }

    static create(socket, canvasElementID, playerElementID, id, name, room_name) {
        const canvas = document.getElementById(canvasElementID)
        const sound = Sound.create(document);

        const drawing = Drawing.create(canvas);
        const playerboard = PlayerBoard.create(playerElementID);

        
        const game = new Game(socket, drawing, playerboard, id, name, room_name, sound);
        game.init();
        return game;
    }

    init() {
       this.self = new Player(this.id, this.name);
       this.state = Constants.STATE_GAME_WAIT;
       this.lastUpdateTime = Date.now();
    }

    onGameInfo(packet) {
        if(this.state === Constants.STATE_GAME_IN) {
            this.self.cards = packet.payload.cards;
            this.self.moves = packet.payload.moves;
            this.players_card_count = packet.payload.player_card_count;
            this.turn = packet.payload.turn;
            this.pile_top = packet.payload.pile_top;
            this.move_time = packet.payload.move_time;
            this.playerboard.update(this.players_card_count, this.turn, this.self);
            this.draw_count = packet.payload.draw_count;

            this.start_time = Date.now();

            this.stackcount = (this.stackcount + 1).mod(5);

            //Die Auswahl Karte wird auf die erste legbare Karte gesetzt
            if(this.turn == this.self.id) {
                if(this.self.moves.length > 0) {
                    this.self.selectedCard = this.self.cards.indexOf(this.self.moves[0]); 
                } else {    //Auswahl auf Ziehstapel
                    this.self.selectedCard = this.self.cards.length;
                }
            }
        }
    }
    /**
     * While waiting in room that other players join, this method is called
     * @param {Packet} packet 
     */
    onGameUpdate(packet) {      
        if(this.state === Constants.STATE_GAME_WAIT) {
            this.room_name = packet.payload.name;
            this.max_players = packet.payload.max_players;
            this.players = packet.payload.players;

            this.playerboard.updateWaitingPlayers(this.players);
        }
    }

    onGameEnd(packet) {
        if(this.state === Constants.STATE_GAME_IN) {
            this.state = Constants.STATE_GAME_END;
            this.winner_id = packet.payload.id;
            this.winner_name = packet.payload.winner;

            //Die Kartenanzahl des Gewinners wird auf Null gesetzt
            this.players_card_count.forEach(player => {
                if (player.id == this.winner_id) {
                    player.cards = 0;
                }
            });
            this.playerboard.update(this.players_card_count, this.turn, this.self);
        }
    }

    gameStarts() {
        if(this.state === Constants.STATE_GAME_WAIT) {
            this.state = Constants.STATE_GAME_IN;
            //this.parent.state = Constants.STATE_GAME;
        } 
        console.log("Game started, all Players are in.")
    }

    drawState() {
        this.drawing.clear();
        
        if((this.state === Constants.STATE_GAME_IN) && this.players_card_count) {
            this.countdown = this.move_time*1000 - (Date.now() - this.start_time)
            if(this.countdown < 0) this.countdown = 0
            this.drawing.drawGameScreen(this.self, this.players_card_count, this.turn, this.pile_top, {countdown: this.countdown, move_time: this.move_time*1000}, this.draw_count, this.stackcount);
        } else if (this.state === Constants.STATE_GAME_WAIT) {
            this.drawing.drawWaitingScreen(this.max_players, this.players);
        }  else if (this.state === Constants.STATE_GAME_END) {
            this.drawing.drawWinnerScreen(this.self, this.winner_id, this.winner_name);
        } 

    }

    run() {
        const currentTime = Date.now()
        this.deltaTime = currentTime - this.lastUpdateTime
        this.lastUpdateTime = currentTime
    
        this.drawing.updateDimensions();
        this.drawState()
        this.animationFrameId = window.requestAnimationFrame(this.run.bind(this))
    }

    stop() {
        window.cancelAnimationFrame(this.animationFrameId)
    }

    receiveKeyInputs(key) {
        if((this.state === Constants.STATE_GAME_IN)) {
            switch(key) {
                case Constants.KEY_LEFT:
                    this.self.selectedCard = (this.self.selectedCard - 1).mod(this.self.cards.length+1); 
                    break;
                case Constants.KEY_RIGHT:
                    this.self.selectedCard = (this.self.selectedCard + 1) % (this.self.cards.length+1);    
                    break;
                case Constants.KEY_UP:
                    this.self.selectedColor = (this.self.selectedColor + 1) % 4;
                    break;

                case Constants.KEY_DOWN:
                    this.self.selectedColor = (this.self.selectedColor - 1).mod(4);
                    break;
                    
                case Constants.KEY_ENTER:
                    if(this.turn === this.self.id) {        //Prüfen ob Spieler am Zug
                        if(this.self.selectedCard < this.self.cards.length) {   //Prüfen ob Karte ausgewählt
                            const nid = ((this.self.cards[this.self.selectedCard]-1).mod(Constants.CARD_COUNT))+1;
                            if(this.self.moves.some(c =>  (c === this.self.cards[this.self.selectedCard])) && !(Constants.CARD_ID_CHOOSE_COLOR.some(c => (c === nid)))) { //PRÜFEN OB SONDER KARTE
                                this.socket.send(Constants.SOCKET_CARD, {card: this.self.cards[this.self.selectedCard], color: "none"});
                            } else if (this.self.moves.some(c =>  (c === this.self.cards[this.self.selectedCard])) && (Constants.CARD_ID_CHOOSE_COLOR.some(c => (c === nid)))) {
                                console.log("You selected an CHOOSING CARD!")
                                this.socket.send(Constants.SOCKET_CARD, {card: this.self.cards[this.self.selectedCard], color: Constants.CARD_COLORS[this.self.selectedColor]});

                            } else {
                                console.log("This card is invalid")
                                if(this.turn == this.self.id) this.sound.play(Constants.SOUND_INVALID);
                                
                            }
                        } else {
                            console.log("Send draw");
                            this.socket.send(Constants.SOCKET_CARD, {card: Constants.CARD_ID_DRAW, color: "none"});

                       }
                    } 
                    break;
                case Constants.KEY_ESC:
                        console.log("ESC pressed")
                        this.socket.send(Constants.SOCKET_GAME_FORCE_WIN, {});
    
                        break;
            }   
        } else if (this.state === Constants.STATE_GAME_WAIT) {
            switch(key) {
                case Constants.KEY_ESC:
                    console.log("ESC pressed")
                    this.exitGame()

                    break;
                case Constants.KEY_ENTER:
                    console.log("ENTER pressed") 

                    this.socket.send(Constants.SOCKET_FORCE_ROOM_START, {});
                    //this.gameStarts();
        
                    break;
            }               
        }  else if (this.state === Constants.STATE_GAME_END) {
            switch(key) {
                case Constants.KEY_ESC:
                    console.log("ESC pressed")
                    this.exitGame()


                    break;
                case Constants.KEY_ENTER:
                    console.log("ENTER pressed")
                    this.exitGame()

        
                    break;
            }               
        }

    }

    onPlaySound(packet) {
        if(packet == "sound_draw") {
            this.sound.play(Constants.SOUND_DRAW);
        } else if (packet == "sound_place") {
            this.sound.play(Constants.SOUND_PLACE);
        } else if(packet == Constants.SOUND_CHAT) {
            this.sound.play(Constants.SOUND_CHAT);
        }
    }

    exitGame() {
        console.log('Exit Game State');
        this.socket.send(Constants.SOCKET_GAME_LEAVE, {});
        this.stop();
        this.parent.exitRoom();
    }

    set setParent(parent) {
        this.parent = parent;
    }

}