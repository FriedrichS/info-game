class Input {
    constructor() {
        this.left  = false;
        this.right = false;
        this.mouseDown = false;
        this.enter = false;
        this.mouseCoords = [0, 0];

        this.parent;
    }

    static create(keyElement, mouseMoveElement) {
        const input = new Input();
        input.applyEventHandler(keyElement, keyElement, mouseMoveElement);
        return input;
    }

    onKeyDown(event) {
        switch (event.keyCode) {
            case Constants.KEY_UP: //UP
                this.parent.receiveKeyInputs(Constants.KEY_UP);
                break;
            case Constants.KEY_DOWN: //DOWN
                this.parent.receiveKeyInputs(Constants.KEY_DOWN);
                break;
            case Constants.KEY_LEFT: //LEFT
                this.parent.receiveKeyInputs(Constants.KEY_LEFT);
                break;
            case Constants.KEY_RIGHT: //RIGHT
                this.parent.receiveKeyInputs(Constants.KEY_RIGHT);
                break;

            case Constants.KEY_ENTER: //ENTER
                this.parent.receiveKeyInputs(Constants.KEY_ENTER);
                break;
            case Constants.KEY_ESC: //ESC
                this.parent.receiveKeyInputs(Constants.KEY_ESC);
                break;
            
        }
    }

    onKeyUp(event) {
        switch (event.keyCode) {
            case 65: //A
            case 68: //D
            case 37: //LEFT
            case 39: //RIGHT
            case 13: //ENTER
            case 27: //ESC
            
        }
    }

    onMouseUp(event) {
        if (event.which === 1) {
            this.mouseDown = true;
        }
    }

    onMouseDown(event) {
        if (event.which === 1) {
            this.mouseDown = false;
        }
    }

    onMouseMove(event) {
        this.mouseCoords = [event.offsetX, event.offsetY]
    }

    applyEventHandler(keyElement, mouseClickElement, mouseMoveElement) {
        keyElement.addEventListener("keydown", this.onKeyDown.bind(this));
        keyElement.addEventListener("keyup", this.onKeyUp.bind(this));
        mouseClickElement.addEventListener("mousedown", this.onMouseDown.bind(this));
        mouseClickElement.addEventListener("mouseup", this.onMouseUp.bind(this));
        mouseMoveElement.setAttribute("tabindex", 1);
        mouseMoveElement.addEventListener("mousemove", this.onMouseMove.bind(this));
    }

    removeEventHandler(keyElement, mouseClickElement, mouseMoveElement) {

    }

    set setParent(parent) {
        this.parent = parent;
    }
}