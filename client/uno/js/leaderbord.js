class PlayerBoard {
    constructor(container) {
        this.container = container;
    }

    static create(containerElementID) {
        return new PlayerBoard(document.getElementById(containerElementID));
    }

    update(player_card_count, turn, self) {
        while (this.container.firstChild) {
            this.container.removeChild(this.container.firstChild)
        }

        const element = document.createElement('div');
        element.setAttribute('class', 'leaderboard-header');
        element.appendChild(document.createTextNode(`Current player:`))
        this.container.appendChild(element);




        player_card_count.forEach(player => {
            const element = document.createElement('div');
            // const image = document.createElement('img');

            element.setAttribute('class', 'leaderboard-div');
            // image.setAttribute('src', 'img/UNO-Back-4.png');
            // image.setAttribute('id', 'back_img');

            if(player.id === self.id) {
                element.setAttribute('class', 'self-node leaderboard-div');
            }


            element.appendChild(document.createTextNode(`${player.name} : ${player.cards} Cards`))

            // element.appendChild(image)
            this.container.appendChild(element);

            if(player.id === turn) {
                const element2 = document.createElement('div');
                element2.setAttribute('class', 'leaderboard-header');
                element2.setAttribute('id', 'leaderboard-next-players');

                element2.appendChild(document.createTextNode(`Next players:`))
                this.container.appendChild(element2);
            }

        });    
        
    }

    updateWaitingPlayers(players) {
        while (this.container.firstChild) {
            this.container.removeChild(this.container.firstChild)
        }

        const element = document.createElement('div');
        element.setAttribute('class', 'leaderboard-header');
        element.appendChild(document.createTextNode(`Joined players:`))
        this.container.appendChild(element);




        players.forEach(player => {
            const element = document.createElement('div');
            // const image = document.createElement('img');

            element.setAttribute('class', 'leaderboard-div');
            // image.setAttribute('src', 'img/UNO-Back-4.png');
            // image.setAttribute('id', 'back_img');



            element.appendChild(document.createTextNode(player))

            // element.appendChild(image)
            this.container.appendChild(element);

            

        });    
        
    }

}