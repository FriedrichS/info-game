class Lobby {
    constructor(socket, name) {
        this.socket = socket;

        this.playerBoard = document.getElementById('playerBoard');
        this.roomBoard = document.getElementById('roomBoard');

        this.inputRoomName = document.getElementById('txtRoomName');
        this.inputPlayerCount = document.getElementById('txtMaxPlayer');

        this.players = [];
        this.rooms = [];

        this.name = name;
        this.id;

        this.game;
        this.chat;
        this.state = Constants.STATE_LOBBY;         //
        this.acknowledged = false;
    }

    static create(socket, name) {
        const canvas = document.getElementById("ctx")

        
        const input = Input.create(canvas, canvas);

        const lobby = new Lobby(socket, name);
        socket.send(Constants.SOCKET_JOIN, {name: name});
        socket.setParent = lobby;
        input.setParent = lobby;

        return lobby;
    }

    // run() {

    // }

    receiveMessage(packet) {
        
        if(packet.name === "p") {
            this.socket.send("p", {});
        } else {
            console.log(packet);
        }

        if(this.state === Constants.STATE_LOBBY) {           //###### LOBBY
            switch(packet.name) {
                case Constants.SOCKET_JOIN_ACK:
                    this.id = packet.payload.id;
                    this.acknowledged = true;
                    break;
                case Constants.SOCKET_LOBBY:
                    this.players = packet.payload.players;
                    this.rooms = packet.payload.rooms;
                    this.updateBoards();
                    break;
                case Constants.SOCKET_GAME_ACK:
                    if(packet.payload.success) this.startGame(packet.payload.name);
                    break;
            }
        } else if(this.state === Constants.STATE_WAIT) {     //####### WAITING
            switch(packet.name) {
                case Constants.SOCKET_GAME_UPDATE:
                    this.game.onGameUpdate(packet);
                    break;
                case Constants.SOCKET_GAME_START:
                    this.state = Constants.STATE_GAME;
                    this.game.gameStarts();
                    break;
                case Constants.SOCKET_CHAT_UPDATE:
                    this.chat.onChatReceive(packet);
                    this.game.onPlaySound(Constants.SOUND_CHAT);
                    break;
            }            
        } else if(this.state === Constants.STATE_GAME) {     //####### GAME
            switch(packet.name) {
                case Constants.SOCKET_GAME_INFO:
                    this.game.onGameInfo(packet);
                    break;
                case Constants.SOCKET_CHAT_UPDATE:
                    this.chat.onChatReceive(packet);
                    this.game.onPlaySound(Constants.SOUND_CHAT);
                    break;
                case Constants.SOCKET_GAME_END:
                    this.game.onGameEnd(packet);
                case Constants.SOCKET_PLAY_SOUND:
                    this.game.onPlaySound(packet.payload.name);

            }            
        }
    }

    receiveKeyInputs(event) {
        if(this.state !== Constants.STATE_LOBBY && this.game != null) {
            this.game.receiveKeyInputs(event)
        }
    }

    updateBoards() {
        this.playerBoard.innerHTML = "";
        
        for(let i = 0; i < this.players.length; i++) {
            this.playerBoard.innerHTML += '<li class="list-group-item"> ' + this.players[i] + '</li>';
        }

        this.roomBoard.innerHTML = '';

        for(let i = 0; i < this.rooms.length; i++) {
            this.roomBoard.innerHTML += '<li class="list-group-item roomList" id="' + this.rooms[i].name +'">' + this.rooms[i].name + ' (' + this.rooms[i].players + '/'  +  this.rooms[i].max_players  + ')' + '</li>';
        }
    }

    joinRoom(room) {
        if(this.state === Constants.STATE_LOBBY && this.acknowledged) {
            const room_name =  room.target.id;

            this.socket.send(Constants.SOCKET_JOIN_GAME, {name: room_name});

        }
    }
    
    startGame(room_name) {
        if(this.state == Constants.STATE_LOBBY) {
            this.state = Constants.STATE_WAIT;
            this.game = Game.create(this.socket, "ctx", "leaderboard-display", this.id, this.name, room_name); //new Game(ctx, canvas, id, playerName);
            this.game.setParent = this;
            this.chat = Chat.create(this.socket, "chat-display", "chat-input");
            console.log("Joined Room")

            $("#lobby").hide();
            $("#game").show();

            this.game.run();
        }
    }

    newRoom() {
        if(this.acknowledged) {
            const room_name = this.inputRoomName.value;
            const max_players = parseInt(this.inputPlayerCount.value);

            if(room_name != "" && max_players != 0){
                this.socket.send(Constants.SOCKET_CREATE_ROOM, {name: room_name, max_players: max_players});
                this.inputRoomName.value = null;
                this.inputPlayerCount.value = null;

                
                console.log("Room created")
    
                //startGame();
            }
        }
    }

    exitRoom() {
        if(this.state !== Constants.STATE_LOBBY) {
            this.state = Constants.STATE_LOBBY;
            this.game = null;
            this.chat = null;

            $("#lobby").show();
            $("#game").hide();

            console.log("Exit Room")
        }

    }

}