class Packet {
    static encode(name, payload) {
        return JSON.stringify({name: name, payload: JSON.stringify(payload)});
    }

    static decode(object) {
        const load = JSON.parse(JSON.parse(object).payload);
        const name = JSON.parse(object).name;
        return {name: name, payload: load};
    }

}
