class Player {
    constructor(id, name) {
        this.cards = [15, 31, 25, 36];
        this.selectedCard = 0;
        this.selectedColor = 0;
        this.id = id;
        this.name = name;
        this.moves = [31, 25];
    }
}
