class Socket {
    constructor(path) {
        this.ws = new WebSocket(path);
        this.messages = []
        this.parent;

        //this.send(Constants.SOCKET_JOIN, {name: this.name});
    }

    static create(path, parent) {
        const socket = new Socket(path);
        socket.applyEventHandler();
        return socket;
    }

    onOpen(event) {
        console.log("Server connected")
    }

    get getStatus() {
        return this.ws.readyState
    }

    set setParent(parent) {
        this.parent = parent;
    }

    onMessage(event) {
        const packet = Packet.decode(event.data);
        this.messages.push(packet);

        //console.log(packet);

        this.parent.receiveMessage(packet);
    }

    onClose(event) {
        console.log("Server disconnected")
    }

    onError(event) {
        console.log('WebSocket Error: ',  event);
    }

    async send(name, payload) {
        if (this.ws.readyState !== this.ws.OPEN) {
            try {
                await waitForOpenConnection(this.ws)
                this.ws.send(Packet.encode(name, payload))
            } catch (err) { console.error(err) }
        } else {
            this.ws.send(Packet.encode(name, payload))
        } 
    }

    // async send(packet) {
    //     if (this.ws.readyState !== this.ws.OPEN) {
    //         try {
    //             await waitForOpenConnection(this.ws)
    //             this.ws.send(packet)
    //         } catch (err) { console.error(err) }
    //     } else {
    //         this.ws.send(packet)
    //     } 
    // }

    close() {
        this.ws.close();
    }

    applyEventHandler() {
        this.ws.addEventListener("open", this.onOpen.bind(this));
        this.ws.addEventListener("message", this.onMessage.bind(this));
        this.ws.addEventListener("close", this.onClose.bind(this));
        this.ws.addEventListener("error", this.onError.bind(this));
    }

}

const waitForOpenConnection = (socket) => {
    return new Promise((resolve, reject) => {
        const maxNumberOfAttempts = 10
        const intervalTime = 200 //ms

        let currentAttempt = 0
        const interval = setInterval(() => {
            if (currentAttempt > maxNumberOfAttempts - 1) {
                clearInterval(interval)
                reject(new Error('Maximum number of attempts exceeded'))
            } else if (socket.readyState === socket.OPEN) {
                clearInterval(interval)
                resolve()
            }
            currentAttempt++
        }, intervalTime)
    })
}