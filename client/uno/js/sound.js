class Sound {
    constructor(placeSound, shuffleSound, drawSound, invalidSound, chatSound){
        this.placeSound = placeSound;
        this.shuffleSound = shuffleSound;
        this.drawSound = drawSound;
        this.invalidSound = invalidSound;
        this.chatSound = chatSound;
    }

    static create(document) {
        const placeSound = document.createElement("audio");
        placeSound.src = Constants.SOUND_BASE_PATH + Constants.SOUND_PLACE;
        placeSound.setAttribute("preload", "auto");
        placeSound.setAttribute("controls", "none");
        placeSound.style.display = "none";
        document.body.appendChild(placeSound);

        const shuffleSound = document.createElement("audio");
        shuffleSound.src = Constants.SOUND_BASE_PATH + Constants.SOUND_SHUFFLE;
        shuffleSound.setAttribute("preload", "auto");
        shuffleSound.setAttribute("controls", "none");
        shuffleSound.style.display = "none";
        document.body.appendChild(shuffleSound);

        const drawSound = document.createElement("audio");
        drawSound.src = Constants.SOUND_BASE_PATH + Constants.SOUND_DRAW;
        drawSound.setAttribute("preload", "auto");
        drawSound.setAttribute("controls", "none");
        drawSound.style.display = "none";
        document.body.appendChild(drawSound);

        const invalidSound = document.createElement("audio");
        invalidSound.src = Constants.SOUND_BASE_PATH + Constants.SOUND_INVALID;
        invalidSound.setAttribute("preload", "auto");
        invalidSound.setAttribute("controls", "none");
        invalidSound.style.display = "none";
        document.body.appendChild(invalidSound);

        const chatSound = document.createElement("audio");
        chatSound.src = Constants.SOUND_BASE_PATH + Constants.SOUND_CHAT;
        chatSound.setAttribute("preload", "auto");
        chatSound.setAttribute("controls", "none");
        chatSound.style.display = "none";
        document.body.appendChild(chatSound);


        return new Sound(placeSound, shuffleSound, drawSound, invalidSound, chatSound);
    }

    play(sound) {
        switch(sound) {
            case Constants.SOUND_PLACE:
                this.placeSound.play();
                break;
            case Constants.SOUND_SHUFFLE:
                this.shuffleSound.play();

                break;
            case Constants.SOUND_DRAW:
                this.drawSound.play();
                break;
            case Constants.SOUND_INVALID:
                this.invalidSound.play();
                break;
            case Constants.SOUND_CHAT:
                this.chatSound.play();
                break;
        }
    }

    stop(sound) {
        switch(sound) {
            case Constants.SOUND_PLACE:
                break;
            case Constants.SOUND_SHUFFLE:
                break;
            case Constants.SOUND_DRAW:
                break;
        }
    }

    removeSounds(document) {
        document.body.removeChild(this.drawSound);
        document.body.removeChild(this.shuffleSound);
        document.body.removeChild(this.placeSound);
    }


}