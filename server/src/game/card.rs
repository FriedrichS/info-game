use rand::Rng;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Clone, PartialEq)]
pub struct Card {
    pub color: Color,
    pub ty: CardType,
    pub id: u32,
}

impl Card {
    pub const fn new(color: Color, ty: CardType) -> Card {
        Card { color, ty, id: 0 }
    }

    pub fn get_draw_count(&self) -> u32 {
        match self.ty {
            CardType::DrawTwo => 2,
            CardType::WildDraw => 4,
            _ => 0,
        }
    }

    pub fn fits_on(&self, other: &Card, to_draw: u32) -> bool {
        match self.ty {
            CardType::Wild => to_draw == 0,
            CardType::WildDraw => true,
            CardType::Num(n1) => {
                if to_draw != 0 {
                    false
                } else {
                    let same_color = self.color == other.color;
                    match other.ty {
                        CardType::Wild | CardType::WildDraw => same_color,
                        CardType::Num(n2) => (n1 == n2) || same_color,
                        CardType::Skip | CardType::Reverse | CardType::DrawTwo => same_color,
                    }
                }
            }
            CardType::Skip | CardType::Reverse => {
                (self.color == other.color || self.ty == other.ty) && to_draw == 0
            }
            CardType::DrawTwo => (self.color == other.color) || other.ty == CardType::DrawTwo,
        }
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let cmp = self.color.partial_cmp(&other.color).unwrap();
        if cmp != Ordering::Equal {
            return Some(cmp);
        }

        Some(self.ty.partial_cmp(&other.ty).unwrap())
    }
}

#[derive(Copy, Clone, PartialEq, PartialOrd, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Color {
    None,
    Red,
    Green,
    Blue,
    Yellow,
}

impl Color {
    pub fn random() -> Color {
        let mut rng = rand::thread_rng();
        match rng.gen_range(0..4u32) {
            0 => Self::Red,
            1 => Self::Green,
            2 => Self::Blue,
            3 => Self::Yellow,
            _ => Self::Red,
        }
    }
}

#[derive(Copy, Clone, PartialEq, PartialOrd)]
pub enum CardType {
    Wild,
    WildDraw,
    Num(u32),
    Skip,
    Reverse,
    DrawTwo,
}

#[cfg(test)]
mod test {
    use super::*;
    use CardType::*;
    use Color::*;

    #[test]
    fn test_cards_fit() {
        // same color - different number
        assert_eq!(
            Card::new(Red, Num(4)).fits_on(&Card::new(Red, Num(7)), 0),
            true
        );
        assert_eq!(
            Card::new(Green, Num(4)).fits_on(&Card::new(Red, Num(7)), 0),
            false
        );

        // different color - same number
        assert_eq!(
            Card::new(Red, Num(5)).fits_on(&Card::new(Green, Num(5)), 0),
            true
        );
        assert_eq!(
            Card::new(Red, Num(6)).fits_on(&Card::new(Green, Num(5)), 0),
            false
        );

        // wild on number card
        assert_eq!(
            Card::new(None, Wild).fits_on(&Card::new(Red, Num(2)), 0),
            true
        );
        assert_eq!(
            Card::new(Blue, Wild).fits_on(&Card::new(Red, Num(3)), 0),
            true
        );

        // draw on number card
        assert_eq!(
            Card::new(Yellow, DrawTwo).fits_on(&Card::new(Red, Num(2)), 0),
            false
        );
        assert_eq!(
            Card::new(Yellow, DrawTwo).fits_on(&Card::new(Yellow, Num(3)), 0),
            true,
        );

        // number card on draw
        assert_eq!(
            Card::new(Red, Num(3)).fits_on(&Card::new(Red, DrawTwo), 2),
            false
        );
        assert_eq!(
            Card::new(Red, Num(5)).fits_on(&Card::new(Red, DrawTwo), 0),
            true
        );

        // skip on draw
        assert_eq!(
            Card::new(Yellow, Skip).fits_on(&Card::new(Red, DrawTwo), 2),
            false
        );

        // reverse on draw
        assert_eq!(
            Card::new(Yellow, Reverse).fits_on(&Card::new(Red, DrawTwo), 2),
            false,
        );

        // reverse on wilddraw
        assert_eq!(
            Card::new(Green, Reverse).fits_on(&Card::new(Green, WildDraw), 4),
            false,
        );
        assert_eq!(
            Card::new(Green, Reverse).fits_on(&Card::new(Red, WildDraw), 4),
            false,
        );

        // reverse on reverse
        assert_eq!(
            Card::new(Green, Reverse).fits_on(&Card::new(Red, Reverse), 0),
            true,
        );
    }
}
