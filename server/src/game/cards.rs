use super::{Card, CardType, Color};
use rand::seq::SliceRandom;

const CARDS: &'static [Card] = &[
    // red cards
    Card::new(Color::Red, CardType::Num(1)),
    Card::new(Color::Red, CardType::Num(2)),
    Card::new(Color::Red, CardType::Num(3)),
    Card::new(Color::Red, CardType::Num(4)),
    Card::new(Color::Red, CardType::Num(5)),
    Card::new(Color::Red, CardType::Num(6)),
    Card::new(Color::Red, CardType::Num(7)),
    Card::new(Color::Red, CardType::Num(8)),
    Card::new(Color::Red, CardType::Num(9)),
    Card::new(Color::Red, CardType::Skip),
    Card::new(Color::Red, CardType::Reverse),
    Card::new(Color::Red, CardType::DrawTwo),
    Card::new(Color::None, CardType::Wild),
    // yellow cards
    Card::new(Color::Yellow, CardType::Num(1)),
    Card::new(Color::Yellow, CardType::Num(2)),
    Card::new(Color::Yellow, CardType::Num(3)),
    Card::new(Color::Yellow, CardType::Num(4)),
    Card::new(Color::Yellow, CardType::Num(5)),
    Card::new(Color::Yellow, CardType::Num(6)),
    Card::new(Color::Yellow, CardType::Num(7)),
    Card::new(Color::Yellow, CardType::Num(8)),
    Card::new(Color::Yellow, CardType::Num(9)),
    Card::new(Color::Yellow, CardType::Skip),
    Card::new(Color::Yellow, CardType::Reverse),
    Card::new(Color::Yellow, CardType::DrawTwo),
    Card::new(Color::None, CardType::Wild),
    // greeen cards
    Card::new(Color::Green, CardType::Num(1)),
    Card::new(Color::Green, CardType::Num(2)),
    Card::new(Color::Green, CardType::Num(3)),
    Card::new(Color::Green, CardType::Num(4)),
    Card::new(Color::Green, CardType::Num(5)),
    Card::new(Color::Green, CardType::Num(6)),
    Card::new(Color::Green, CardType::Num(7)),
    Card::new(Color::Green, CardType::Num(8)),
    Card::new(Color::Green, CardType::Num(9)),
    Card::new(Color::Green, CardType::Skip),
    Card::new(Color::Green, CardType::Reverse),
    Card::new(Color::Green, CardType::DrawTwo),
    Card::new(Color::None, CardType::WildDraw),
    // blue cards
    Card::new(Color::Blue, CardType::Num(1)),
    Card::new(Color::Blue, CardType::Num(2)),
    Card::new(Color::Blue, CardType::Num(3)),
    Card::new(Color::Blue, CardType::Num(4)),
    Card::new(Color::Blue, CardType::Num(5)),
    Card::new(Color::Blue, CardType::Num(6)),
    Card::new(Color::Blue, CardType::Num(7)),
    Card::new(Color::Blue, CardType::Num(8)),
    Card::new(Color::Blue, CardType::Num(9)),
    Card::new(Color::Blue, CardType::Skip),
    Card::new(Color::Blue, CardType::Reverse),
    Card::new(Color::Blue, CardType::DrawTwo),
    Card::new(Color::None, CardType::WildDraw),
];

pub struct Cards;

impl Cards {
    pub fn get_card(mut idx: usize) -> Card {
        idx -= 1;
        let ranged_idx = idx % CARDS.len();
        let mut card = CARDS[ranged_idx].clone();
        card.id = (idx + 1) as u32;
        card
    }

    /// Creates and returns a shuffled deck
    /// For a default uno deck: `num_decks = 2`
    pub fn create_deck(num_decks: usize) -> Vec<Card> {
        let mut deck = Vec::with_capacity(num_decks * CARDS.len());
        let mut card_id = 1;
        for _ in 0..num_decks {
            for card in CARDS {
                let mut card = card.clone();
                card.id = card_id;
                card_id += 1;
                deck.push(card);
            }
        }

        let mut rng = rand::thread_rng();
        deck.shuffle(&mut rng);

        deck
    }
}
