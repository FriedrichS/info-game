use crate::{game, net::*, register_msg, LOBBY, LOG};
use parking_lot::RwLock;
use slog::*;
use std::sync::Arc;
use tokio::{net::TcpStream, sync::Mutex};
use tokio_tungstenite::WebSocketStream;

pub struct Client {
    pub id: RwLock<u32>,
    pub name: RwLock<String>,
    pub room: RwLock<Option<Arc<Mutex<game::Room>>>>,
    pub socket: Arc<Socket>,
}

impl Client {
    pub async fn new(id: u32, stream: WebSocketStream<TcpStream>) -> Arc<Client> {
        let socket = Arc::new(Socket::new(
            stream,
            Some(move || {
                tokio::spawn(async move {
                    let mut lobby = LOBBY.lock().await;
                    lobby.disconnect_client(id).await;
                });
            }),
        ));
        let client = Arc::new(Client {
            id: RwLock::new(id),
            name: RwLock::new(String::new()),
            room: RwLock::new(None),
            socket,
        });

        register_msg!(client, JoinPacket, on_join);
        register_msg!(client, CreateRoomPacket, on_create_room);
        register_msg!(client, JoinRoomPacket, on_join_room);
        register_msg!(client, LeaveRoomPacket, on_leave_room);
        register_msg!(client, CardPlayPacket, on_card_play);
        register_msg!(client, ChatEmitPacket, on_chat_emit);
        register_msg!(client, ForceRoomStartPacket, on_force_start_room);
        register_msg!(client, ForceWinPacket, on_force_win);

        Socket::connect(Arc::clone(&client.socket)).await;

        client
    }

    async fn on_join(&self, msg: JoinPacket) {
        *self.name.write() = msg.name;

        let id = *self.id.read();
        self.socket
            .send(JoinAckPacket::NAME, &JoinAckPacket { id })
            .await;

        let lobby = LOBBY.lock().await;
        lobby.send_lobby_update().await;
    }

    async fn on_create_room(&self, msg: CreateRoomPacket) {
        let mut lobby = LOBBY.lock().await;
        let id = *self.id.read();
        lobby.create_room(msg.name, msg.max_players, id).await;
    }

    async fn on_join_room(&self, msg: JoinRoomPacket) {
        let mut lobby = LOBBY.lock().await;
        let id = *self.id.read();
        lobby.move_to_room(id, &msg.name).await;
    }

    async fn on_leave_room(&self, _: LeaveRoomPacket) {
        let room = self.room.read().as_ref().cloned();
        if let Some(room) = room {
            let mut lobby = LOBBY.lock().await;
            let id = *self.id.read();
            lobby.move_to_lobby(id, room).await;
        }
    }

    async fn on_card_play(&self, msg: CardPlayPacket) {
        let room = self.room.read().as_ref().cloned();
        if let Some(room) = room {
            let id = *self.id.read();
            room.lock().await.play_card(id, msg.card, msg.color).await;
        }
    }

    async fn on_chat_emit(&self, msg: ChatEmitPacket) {
        let packet = ChatUpdatePacket {
            name: self.name.read().to_owned(),
            msg: msg.msg,
        };

        let room = self.room.read().as_ref().cloned();
        if let Some(room) = room {
            let room = room.lock().await;
            for user in room.connected.values() {
                user.socket.send(ChatUpdatePacket::NAME, &packet).await;
            }
        } else {
            let lobby = LOBBY.lock().await;
            for user in lobby.connected.values() {
                user.socket.send(ChatUpdatePacket::NAME, &packet).await;
            }
        }
    }

    async fn on_force_start_room(&self, _: ForceRoomStartPacket) {
        let room = self.room.read().as_ref().cloned();
        if let Some(room) = room {
            let mut room = room.lock().await;
            room.max_players = room.connected.len() as u32;
            room.try_start().await;
        }
    }

    async fn on_force_win(&self, _: ForceWinPacket) {
        let room = self.room.read().as_ref().cloned();
        if let Some(room) = room {
            let mut room = room.lock().await;
            let name = self.name.read().to_owned();
            let id = *self.id.read();
            room.on_win(name, id).await;
        }
    }
}

#[macro_export]
macro_rules! register_msg {
    ($client: ident, $name: ty, $func: ident) => {
        let client = Arc::clone(&$client);
        $client
            .socket
            .on(<$name>::NAME, move |msg| {
                let client = Arc::clone(&client);
                match serde_json::from_str(&msg) {
                    Ok(msg) => {
                        tokio::spawn(async move { client.$func(msg).await });
                    }
                    Err(e) => error!(LOG, "Error decoding packet: {:?}", e),
                };
            })
            .await;
    };
}
