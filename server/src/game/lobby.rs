use super::{Client, Room};
use crate::{
    net::{self, LobbyUpdatePacket},
    LOG,
};
use net::{JoinRoomAckPacket, PingPacket, RoomUpdatePacket};
use slog::*;
use std::{collections::HashMap, sync::Arc};
use tokio::{net::TcpStream, sync::Mutex};
use tokio_tungstenite::WebSocketStream;

pub struct Lobby {
    pub connected: HashMap<u32, Arc<Client>>,
    pub rooms: HashMap<String, Arc<Mutex<Room>>>,
    clients: HashMap<u32, Arc<Client>>,
    current_id: u32,
}

impl Lobby {
    pub fn new() -> Lobby {
        Lobby {
            connected: HashMap::new(),
            rooms: HashMap::new(),
            clients: HashMap::new(),
            current_id: 0,
        }
    }

    pub async fn add_client(&mut self, socket: WebSocketStream<TcpStream>) {
        let next_id = self.current_id;
        self.current_id += 1;

        let client = Client::new(next_id, socket).await;
        self.connected.insert(next_id, Arc::clone(&client));
        self.clients.insert(next_id, client);

        info!(LOG, "Client {} connected to lobby", next_id);
    }

    pub async fn create_room(&mut self, name: String, max_players: u32, player_id: u32) {
        self.rooms.insert(
            name.to_owned(),
            Arc::new(Mutex::new(Room::new(max_players, name.to_owned()))),
        );
        self.move_to_room(player_id, &name).await;
    }

    pub async fn move_to_room(&mut self, id: u32, room_name: &String) {
        if let Some(client) = self.connected.remove(&id) {
            if let Some(room) = self.rooms.get(room_name) {
                let can_fit = {
                    let room = room.lock().await;
                    (room.connected.len() < (room.max_players as usize)) && !room.ended
                };

                let ack_msg = JoinRoomAckPacket {
                    name: room_name.to_owned(),
                    success: can_fit,
                };
                client.socket.send(JoinRoomAckPacket::NAME, &ack_msg).await;

                if !can_fit {
                    self.connected.insert(id, client);
                    return;
                }

                {
                    let mut room = room.lock().await;
                    room.connected.insert(id, Arc::clone(&client));
                }

                {
                    let mut r = client.room.write();
                    *r = Some(Arc::clone(room));
                }

                self.send_lobby_update().await;
                self.send_room_update(room).await;

                {
                    let mut room = room.lock().await;
                    room.try_start().await;
                }
            }
        }
    }

    pub async fn move_to_lobby(&mut self, id: u32, room: Arc<Mutex<Room>>) {
        let client = room.lock().await.connected.remove(&id).unwrap();
        self.connected.insert(id, Arc::clone(&client));

        {
            let mut r = client.room.write();
            *r = None;
        }

        {
            let room = room.lock().await;
            if room.connected.is_empty() {
                self.rooms.remove(&room.name);
            }
        }

        self.send_lobby_update().await;
        self.send_room_update(&room).await;
    }

    pub async fn disconnect_client(&mut self, id: u32) {
        let client = self.clients.get(&id).cloned();

        if let Some(client) = client {
            let room = client.room.read().as_ref().cloned();

            if let Some(room) = room {
                let mut room = room.lock().await;
                room.connected.remove(&id);

                if room.connected.is_empty() {
                    self.rooms.remove(&room.name);
                }
            } else {
                self.connected.remove(&id);
            }

            self.send_lobby_update().await;
            info!(LOG, "Client {} disconnected from lobby", client.id.read());
        }
    }

    pub async fn process(&mut self, dt: f32) {
        for room in self.rooms.values() {
            room.lock().await.process(dt).await;
        }

        for client in self.connected.values() {
            client.socket.send(PingPacket::NAME, &PingPacket {}).await;
        }
    }

    pub async fn send_lobby_update(&self) {
        let mut players = Vec::new();
        for c in self.connected.values() {
            players.push(c.name.read().to_owned());
        }
        let mut rooms = Vec::new();
        for (name, room) in &self.rooms {
            let room = room.lock().await;
            rooms.push(net::Room {
                name: name.to_owned(),
                max_players: room.max_players,
                players: room.connected.len() as u32,
            });
        }

        let packet = LobbyUpdatePacket { players, rooms };
        for c in self.connected.values() {
            c.socket.send(LobbyUpdatePacket::NAME, &packet).await;
        }
        for room in self.rooms.values() {
            for c in room.lock().await.connected.values() {
                c.socket.send(LobbyUpdatePacket::NAME, &packet).await;
            }
        }
    }

    async fn send_room_update(&self, room: &Arc<Mutex<Room>>) {
        let room = room.lock().await;
        let mut players = Vec::new();
        for c in room.connected.values() {
            players.push(c.name.read().to_owned());
        }

        let packet = RoomUpdatePacket {
            name: room.name.to_owned(),
            max_players: room.max_players,
            players,
        };
        for c in room.connected.values() {
            c.socket.send(RoomUpdatePacket::NAME, &packet).await;
        }
    }
}
