use super::{Card, CardType, Cards, Client, Color};
use crate::{
    net::{
        CardCount, PileTop, PingPacket, PlaySoundPacket, RoomEndPacket, RoomInfoPacket,
        RoomStartPacket,
    },
    util::modulo,
};
use rand::prelude::SliceRandom;
use std::{collections::HashMap, sync::Arc};

const MOVE_TIME: f32 = 20.0;

pub struct Room {
    pub connected: HashMap<u32, Arc<Client>>,
    pub max_players: u32,
    pub name: String,
    deck: Vec<Card>,
    pile: Vec<Card>,
    pub running: bool,
    pub ended: bool,
    players: Vec<PlayerInfo>,
    turn_idx: usize,
    direction: i32,
    current_draw_count: u32, // how many cards the next player would have to draw
    move_time: f32,          // how much time is left to make the current move
}

impl Room {
    pub fn new(max_players: u32, name: String) -> Room {
        Room {
            connected: HashMap::new(),
            max_players,
            name,
            deck: Vec::new(),
            pile: Vec::new(),
            running: false,
            ended: false,
            players: Vec::new(),
            turn_idx: 0,
            direction: 1,
            current_draw_count: 0,
            move_time: MOVE_TIME,
        }
    }

    pub async fn try_start(&mut self) {
        if self.running {
            return;
        }
        if self.connected.len() != (self.max_players as usize) {
            return;
        }
        self.running = true;

        self.deck = Cards::create_deck(2);

        for client in self.connected.values() {
            self.players.push(PlayerInfo::new(
                *client.id.read(),
                client.name.read().to_owned(),
            ));
        }

        // 7 cards per player
        for _ in 0..7 {
            for player in &mut self.players {
                player.deck.push(self.deck.pop().unwrap());
            }
        }

        for player in &mut self.players {
            player.sort_cards();
        }

        // loop as long as there is no draw or wilddraw card
        self.current_draw_count = 1;
        while self.current_draw_count != 0 {
            let mut first_card = self.deck.pop().unwrap();
            if first_card.color == Color::None {
                first_card.color = Color::random();
            }

            self.current_draw_count = first_card.get_draw_count();

            self.pile.push(first_card);
        }

        for c in self.connected.values() {
            c.socket
                .send(RoomStartPacket::NAME, &RoomStartPacket {})
                .await;
        }

        self.send_info().await;
        self.on_next_move();
    }

    pub async fn play_card(&mut self, player_id: u32, id: u32, color: Color) {
        if self.players[self.turn_idx].client_id != player_id {
            return;
        }

        let num_players = self.players.len() as i32;
        let player_idx = self.turn_idx;

        let mut draw_count = self.current_draw_count;
        let cards = self.get_valid_card_count(&self.players[player_idx]);
        if cards == 0 || id == 0 {
            if draw_count == 0 {
                draw_count = 1;
            }
        }
        if draw_count != 0 && cards != 0 && id != 0 {
            draw_count = 0;
        }

        let mut winner = None;
        let player = &mut self.players[player_idx];
        if draw_count == 0 {
            let card_idx = player.deck.iter().position(|c| c.id == id);
            if let Some(card_idx) = card_idx {
                let mut card = player.deck.remove(card_idx);
                self.current_draw_count += card.get_draw_count();

                match card.ty {
                    CardType::Reverse => {
                        if num_players == 2 {
                            self.turn_idx =
                                Self::next_player(self.turn_idx, self.direction, num_players);
                        } else {
                            self.direction = if self.direction == 1 { -1 } else { 1 };
                        }
                    }
                    CardType::Skip => {
                        self.turn_idx =
                            Self::next_player(self.turn_idx, self.direction, num_players);
                    }
                    CardType::Wild | CardType::WildDraw => card.color = color,
                    _ => (),
                };

                self.pile.push(card);

                // player has won
                if player.deck.is_empty() {
                    winner = Some((player.client_name.to_owned(), player.client_id));
                }
            }
        } else {
            for _ in 0..draw_count {
                if self.deck.is_empty() {
                    let pile_top = self.pile.pop().unwrap();
                    for card in self.pile.drain(..) {
                        self.deck.push(card);
                    }
                    self.pile.push(pile_top);

                    let mut rng = rand::thread_rng();
                    self.deck.shuffle(&mut rng);
                }

                player.deck.push(self.deck.pop().unwrap());
            }
            self.current_draw_count = 0;

            let client = self.connected.get(&player.client_id).unwrap();
            let packet = PlaySoundPacket {
                name: "sound_draw".into(),
                play_count: draw_count,
            };
            client.socket.send(PlaySoundPacket::NAME, &packet).await;
        }
        player.sort_cards();

        self.turn_idx = Self::next_player(self.turn_idx, self.direction, num_players);

        if let Some((winner, id)) = winner {
            self.on_win(winner, id).await;
            return;
        }

        let packet = PlaySoundPacket {
            name: "sound_place".into(),
            play_count: 1,
        };
        for client in self.connected.values() {
            client.socket.send(PlaySoundPacket::NAME, &packet).await;
        }

        self.send_info().await;
        self.on_next_move();
    }

    pub async fn process(&mut self, dt: f32) {
        for client in self.connected.values() {
            client.socket.send(PingPacket::NAME, &PingPacket {}).await;
        }

        if !self.running {
            return;
        }

        self.move_time -= dt;
        if self.move_time <= 0.0 {
            let id = self.players[self.turn_idx].client_id;
            self.play_card(id, 0, Color::None).await;
        }
    }

    pub async fn on_win(&mut self, winner: String, id: u32) {
        let packet = RoomEndPacket { winner, id };

        for client in self.connected.values() {
            client.socket.send(RoomEndPacket::NAME, &packet).await;
        }

        self.running = false;
        self.ended = true;
    }

    async fn send_info(&self) {
        for (info, client) in self.players.iter().zip(self.connected.values()) {
            let mut player_card_count = Vec::new();

            for i in 0..self.players.len() {
                let offset = (i as i32) * self.direction;
                let idx = Self::next_player(self.turn_idx, offset, self.players.len() as i32);
                let info = &self.players[idx];

                player_card_count.push(CardCount {
                    id: info.client_id,
                    name: info.client_name.to_owned(),
                    cards: info.deck.len() as u32,
                });
            }

            let pile_top = self.pile.last().unwrap();
            let packet = RoomInfoPacket {
                cards: info.deck.iter().map(|c| c.id).collect(),
                player_card_count,
                moves: self.get_valid_cards(info),
                turn: self.players[self.turn_idx].client_id,
                pile_top: PileTop {
                    id: pile_top.id,
                    color: pile_top.color,
                },
                move_time: MOVE_TIME,
                draw_count: self.current_draw_count,
            };

            client.socket.send(RoomInfoPacket::NAME, &packet).await;
        }
    }

    fn get_valid_cards(&self, player: &PlayerInfo) -> Vec<u32> {
        let mut moves = Vec::new();

        let current = self.pile.last().unwrap();
        for card in &player.deck {
            if card.fits_on(current, self.current_draw_count) {
                moves.push(card.id);
            }
        }

        moves
    }

    fn get_valid_card_count(&self, player: &PlayerInfo) -> u32 {
        let mut moves = 0;

        let current = self.pile.last().unwrap();
        for card in &player.deck {
            if card.fits_on(current, self.current_draw_count) {
                moves += 1;
            }
        }

        moves
    }

    fn on_next_move(&mut self) {
        self.move_time = MOVE_TIME;
    }

    fn next_player(turn_idx: usize, direction: i32, player_count: i32) -> usize {
        modulo((turn_idx as i32) + direction, player_count) as usize
    }
}

struct PlayerInfo {
    client_id: u32,
    client_name: String,
    deck: Vec<Card>,
}

impl PlayerInfo {
    fn new(client_id: u32, client_name: String) -> PlayerInfo {
        PlayerInfo {
            client_id,
            client_name,
            deck: Vec::new(),
        }
    }

    fn sort_cards(&mut self) {
        self.deck.sort_by(|c1, c2| c1.partial_cmp(c2).unwrap());
    }
}
