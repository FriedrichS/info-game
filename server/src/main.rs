use game::Lobby;
use lazy_static::lazy_static;
//use native_tls::{Identity, TlsAcceptor};
use slog::*;
use std::{
    fs::OpenOptions,
    io::{self},
    sync::Arc,
    time::Duration,
};
use tokio::{net::TcpListener, sync::Mutex};
use tokio_tungstenite::accept_async;

mod game;
mod net;
mod util;

lazy_static! {
    pub static ref LOBBY: Mutex<Lobby> = Mutex::new(Lobby::new());
    pub static ref LOG: Arc<Logger> = Arc::new({
        let log_path = "/var/log/uno-server.log";
        let file = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(log_path)
            .expect("Unable to open/create log file");

        let decorator = slog_term::PlainDecorator::new(file);
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        let drain = slog_async::Async::new(drain).build().fuse();

        slog::Logger::root(drain, o!())
    });
}

// In seconds
const PROCESS_DELAY: f32 = 1.0;

#[tokio::main]
async fn main() -> io::Result<()> {
    info!(LOG, "Server started on port 8025");
    let listener = TcpListener::bind("127.0.0.1:8025").await?;

    /*let mut file = File::open("identity.pfx").unwrap();
    let mut identity = vec![];
    file.read_to_end(&mut identity).unwrap();
    let identity = Identity::from_pkcs12(&identity, "password").unwrap();
    let acceptor = TlsAcceptor::new(identity).unwrap();*/

    tokio::spawn(async {
        loop {
            tokio::time::sleep(Duration::from_secs_f32(PROCESS_DELAY)).await;

            {
                let mut lobby = LOBBY.lock().await;
                lobby.process(PROCESS_DELAY).await;
            }
        }
    });

    loop {
        let (stream, addr) = listener.accept().await?;
        info!(LOG, "Incoming connection from {}", addr);

        if let Ok(ws) = accept_async(stream).await {
            LOBBY.lock().await.add_client(ws).await;
        }
    }
}
