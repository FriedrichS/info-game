use serde::{Deserialize, Serialize};

use crate::game::Color;

#[derive(Serialize, Deserialize)]
pub struct JoinPacket {
    pub name: String,
}
impl JoinPacket {
    pub const NAME: &'static str = "join";
}

#[derive(Serialize, Deserialize)]
pub struct JoinAckPacket {
    pub id: u32,
}
impl JoinAckPacket {
    pub const NAME: &'static str = "join_ack";
}

#[derive(Serialize, Deserialize)]
pub struct LobbyUpdatePacket {
    pub players: Vec<String>,
    pub rooms: Vec<Room>,
}
impl LobbyUpdatePacket {
    pub const NAME: &'static str = "lobby";
}

#[derive(Serialize, Deserialize)]
pub struct Room {
    pub name: String,
    pub max_players: u32,
    pub players: u32,
}

#[derive(Serialize, Deserialize)]
pub struct CreateRoomPacket {
    pub name: String,
    pub max_players: u32,
}
impl CreateRoomPacket {
    pub const NAME: &'static str = "create_room";
}

#[derive(Serialize, Deserialize)]
pub struct JoinRoomPacket {
    pub name: String,
}
impl JoinRoomPacket {
    pub const NAME: &'static str = "join_room";
}

#[derive(Serialize, Deserialize)]
pub struct LeaveRoomPacket {}
impl LeaveRoomPacket {
    pub const NAME: &'static str = "leave_room";
}

#[derive(Serialize, Deserialize)]
pub struct RoomInfoPacket {
    pub cards: Vec<u32>,
    pub player_card_count: Vec<CardCount>,
    pub moves: Vec<u32>,
    pub turn: u32,
    pub pile_top: PileTop,
    pub move_time: f32,
    pub draw_count: u32,
}
impl RoomInfoPacket {
    pub const NAME: &'static str = "room_info";
}

#[derive(Serialize, Deserialize)]
pub struct PileTop {
    pub id: u32,
    pub color: Color,
}

#[derive(Serialize, Deserialize)]
pub struct CardCount {
    pub id: u32,
    pub name: String,
    pub cards: u32,
}

#[derive(Serialize, Deserialize)]
pub struct CardPlayPacket {
    pub card: u32,
    pub color: Color,
}
impl CardPlayPacket {
    pub const NAME: &'static str = "card";
}

#[derive(Serialize, Deserialize)]
pub struct ChatEmitPacket {
    pub msg: String,
}
impl ChatEmitPacket {
    pub const NAME: &'static str = "chat_emit";
}

#[derive(Serialize, Deserialize)]
pub struct ChatUpdatePacket {
    pub name: String,
    pub msg: String,
}
impl ChatUpdatePacket {
    pub const NAME: &'static str = "chat_update";
}

#[derive(Serialize, Deserialize)]
pub struct RoomUpdatePacket {
    pub name: String,
    pub max_players: u32,
    pub players: Vec<String>,
}
impl RoomUpdatePacket {
    pub const NAME: &'static str = "room";
}

#[derive(Serialize, Deserialize)]
pub struct RoomStartPacket {}
impl RoomStartPacket {
    pub const NAME: &'static str = "room_start";
}

#[derive(Serialize, Deserialize)]
pub struct JoinRoomAckPacket {
    pub name: String,
    pub success: bool,
}
impl JoinRoomAckPacket {
    pub const NAME: &'static str = "join_room_ack";
}

#[derive(Serialize, Deserialize)]
pub struct ForceRoomStartPacket {}
impl ForceRoomStartPacket {
    pub const NAME: &'static str = "room_start_force";
}

#[derive(Serialize, Deserialize)]
pub struct PingPacket {}
impl PingPacket {
    pub const NAME: &'static str = "p";
}

#[derive(Serialize, Deserialize)]
pub struct RoomEndPacket {
    pub winner: String,
    pub id: u32,
}
impl RoomEndPacket {
    pub const NAME: &'static str = "room_end";
}

#[derive(Serialize, Deserialize)]
pub struct ForceWinPacket {}
impl ForceWinPacket {
    pub const NAME: &'static str = "force_win";
}

#[derive(Serialize, Deserialize)]
pub struct PlaySoundPacket {
    pub name: String,
    pub play_count: u32,
}
impl PlaySoundPacket {
    pub const NAME: &'static str = "play_sound";
}
