use super::Packet;
use crate::LOG;
use futures_util::{
    stream::{SplitSink, SplitStream},
    SinkExt, StreamExt,
};
use serde::Serialize;
use slog::*;
use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};
use tokio::{
    net::TcpStream,
    sync::{Mutex, RwLock},
};
use tokio_tungstenite::{
    tungstenite::{Error, Message},
    WebSocketStream,
};

type Callbacks = HashMap<String, Box<dyn Fn(String) + Send + Sync>>;

pub struct Socket {
    callbacks: Arc<RwLock<Callbacks>>,
    write_sock: Mutex<SplitSink<WebSocketStream<TcpStream>, Message>>,
    read_sock: Mutex<Option<SplitStream<WebSocketStream<TcpStream>>>>,
    on_close: Option<Arc<dyn Fn() + Send + Sync>>,
    running: Arc<AtomicBool>,
}

impl Socket {
    pub fn new<F>(sock: WebSocketStream<TcpStream>, on_close: Option<F>) -> Socket
    where
        F: 'static + Fn() + Send + Sync,
    {
        let (write, read) = sock.split();

        Socket {
            callbacks: Arc::new(RwLock::new(HashMap::new())),
            write_sock: Mutex::new(write),
            read_sock: Mutex::new(Some(read)),
            on_close: match on_close {
                Some(f) => Some(Arc::new(f)),
                None => None,
            },
            running: Arc::new(AtomicBool::new(false)),
        }
    }

    pub async fn connect(socket: Arc<Socket>) {
        if socket.running.load(Ordering::Relaxed) {
            return;
        }

        socket.running.store(true, Ordering::Relaxed);

        let c = Arc::clone(&socket.callbacks);
        let mut sock = {
            let mut lock = socket.read_sock.lock().await;
            std::mem::replace(&mut *lock, None).unwrap()
        };
        let socket = Arc::clone(&socket);
        let running = Arc::clone(&socket.running);
        let on_close = socket.on_close.as_ref().cloned();
        tokio::spawn(async move {
            let mut buf: Vec<u8> = vec![0; 1024 * 64]; // 64 kB of buffer space

            while running.load(Ordering::Relaxed) {
                if let Some(raw) = sock.next().await {
                    match raw {
                        Ok(raw) => match &raw {
                            Message::Text(raw) => {
                                let packet: serde_json::Result<Packet> = serde_json::from_str(raw);
                                if let Ok(packet) = packet {
                                    if packet.name != "p" {
                                        info!(LOG, "Received packet: {}", raw);
                                    }

                                    let callbacks = c.read().await;
                                    if let Some(cb) = callbacks.get(&packet.name) {
                                        cb(packet.payload);
                                    }
                                } else {
                                    error!(LOG, "error: {:?}", packet.err().unwrap());
                                }
                            }
                            Message::Close(_) => {
                                warn!(LOG, "Closing connection: received close packet");
                                running.store(false, Ordering::Relaxed);
                            }
                            _ => (),
                        },
                        Err(e) => match e {
                            Error::ConnectionClosed
                            | Error::AlreadyClosed
                            | Error::Io(_)
                            | Error::Protocol(_)
                            | Error::Url(_) => {
                                warn!(LOG, "Closing connection - error: {}", e);
                                running.store(false, Ordering::Relaxed);
                            }
                            _ => (),
                        },
                    }
                } else {
                    warn!(LOG, "Closing connection: no next message received");
                    running.store(false, Ordering::Relaxed);
                }
                buf.clear();
            }

            if let Some(on_close) = &on_close {
                on_close();
            }
        });
    }

    pub async fn on<F>(&self, msg: &str, callback: F)
    where
        F: 'static + Fn(String) + Send + Sync,
    {
        let mut callbacks = self.callbacks.write().await;
        callbacks.insert(msg.to_owned(), Box::new(callback));
    }

    pub async fn send<P, S>(&self, ty: S, packet: &P)
    where
        P: Serialize,
        S: ToString,
    {
        if !self.running.load(Ordering::Relaxed) {
            return;
        }

        let packet = Packet {
            name: ty.to_string(),
            payload: serde_json::to_string(packet).unwrap(),
        };
        let msg = serde_json::to_string(&packet).unwrap();

        if let Err(e) = self.write_sock.lock().await.send(Message::Text(msg)).await {
            error!(LOG, "Unable to send packet: {:?}", e);
        }
    }

    pub fn close(&self) {
        warn!(LOG, "Force closing socket connection");
        self.running.store(false, Ordering::Relaxed);
    }
}

impl Drop for Socket {
    fn drop(&mut self) {
        self.close()
    }
}
