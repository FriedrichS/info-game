pub fn modulo(a: i32, b: i32) -> i32 {
    let remainder = a % b;
    if remainder < 0 {
        remainder + b
    } else {
        remainder
    }
}
